%% SDR Testbed - Presentation
%% Copyright (C) 2021 Libre Space Foundation
%%
%% This work is licensed under a
%% Creative Commons Attribution-ShareAlike 4.0 International License.
%%
%% You should have received a copy of the license along with this
%% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

\documentclass[english]{beamer}

\mode <presentation>{ % chktex 1
  \usetheme{Boadilla}
  \setbeamercovered{transparent}
}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage{tikz}
\usepackage[siunitx]{circuitikz}
\usepackage{caption}
\usepackage{fancyvrb}
\usepackage{listings}
\usetikzlibrary{arrows,positioning,fit}

% suppress the navigation bar
\beamertemplatenavigationsymbolsempty{}

\renewcommand{\arraystretch}{1.5}

\setlength{\arrayrulewidth}{1pt}
\setlength{\parskip}{3pt}

\title[SDR Testbed]{Software Defined Radio Testbed}
\author{Vasilis Tsiligiannis}
\institute[LSF]{\textbf{Libre Space Foundation}}
\date{September 8, 2021}

\begin{document}

\begin{frame}
  \titlepage{}
\end{frame}

\section{Introduction}

\begin{frame}

  \frametitle{Introduction}

  An SDR testbed is a tool for \ldots{}
  \begin{itemize}
  \item Early experimentation and testing of new algorithms and configurations
  \item Research and development of SDR-based satellite communication systems
  \end{itemize}

  Objective
  \begin{itemize}
  \item Physically implement an SDR testbed
  \item Make it available to Software Defined Radio developers and researchers.
  \end{itemize}

  Design principles
  \begin{itemize}
  \item Based on COTS hardware
  \item Use of free and open source software and technologies
  \item Fully open source
  \item Freely and publicly available as a service to the open source SDR community.
  \end{itemize}

\end{frame}


\section{Hardware Technologies and Components}

\begin{frame}

  \frametitle{Hardware Technologies and Components}

  \begin{figure}[H]
    \centering
    \includegraphics[height=0.5\textheight]{images/sdr-testbed.jpg}
    \caption*{\scriptsize{SDR Testbed RF Components}}
  \end{figure}

  \begin{itemize}
  \item AMD Ryzen 7 2700X COTS computer
  \item Various SDR devices
  \item Programmable RF Components
  \item Fixed RF Components
  \end{itemize}

\end{frame}


\section{Hardware Block Diagram}

\begin{frame}

  \frametitle{Hardware Block Diagram}

  \begin{figure}[H]
    \centering
    \begin{tikzpicture}[node distance=1cm,auto,scale=0.7,transform shape]
      \tikzset{
        rectroundnode/.style={rectangle,rounded corners,draw=black,very thick,inner sep=0.5em,minimum height=3em,text width=2.5cm,text centered},
        arrowlabel/.style={text width=7em},
        arrow/.style={<->,>=latex',shorten >=1pt,thick}
      }
      \node[rectroundnode,minimum width=15em](host){Computer host};
      \node[rectroundnode,below=of host,minimum width=15em](hub){USB/Ethernet hub};
      \node[rectroundnode,below=of hub,text width=1cm](sdr2){SDR \#2};
      \node[rectroundnode,left=0.5cm of sdr2,text width=1cm](sdr1){SDR \#1};
      \node[rectroundnode,right=0.5cm of sdr2,text width=1cm](sdrn){SDR \#N};
      \node[rectroundnode,below=of sdr2,minimum width=15em](progswitch){Programmable switch};
      \node[rectroundnode,below=of progswitch,minimum width=15em](progatt){Programmable attenuator};

      \draw[arrow](host.south) -- node[arrowlabel,right]{\small control \& data} (hub.north);
      \draw[arrow](hub.south) -- node[arrowlabel,right]{\rotatebox{90}{\small data}} (sdr2.north);
      \draw[arrow](hub.south -| sdr1.north) -- node[arrowlabel,right]{\rotatebox{90}{\small data}} (sdr1.north);
      \draw[arrow](hub.south -| sdrn.north) -- node[arrowlabel,right]{\rotatebox{90}{\small data}} (sdrn.north);
      \draw[arrow](sdr2.south) -- node[arrowlabel,right]{\rotatebox{90}{\small RF}} (progswitch.north);
      \draw[arrow](sdr1.south) -- node[arrowlabel,right]{\rotatebox{90}{\small RF}} (progswitch.north -| sdr1.south);
      \draw[arrow](sdrn.south) -- node[arrowlabel,right]{\rotatebox{90}{\small RF}} (progswitch.north -| sdrn.south);
      \draw[arrow](progswitch.south) -- node[arrowlabel,right]{\rotatebox{90}{\small RF}} (progatt.north);
      \draw[arrow,<-](progswitch.west) -| ++(-1cm,0) node[arrowlabel,below,text centered]{\small control} |- (hub.west);
      \draw[arrow,<-](progatt.east) -| ++(1cm,0) node[arrowlabel,below,text centered]{\small control} |- (hub.east);
    \end{tikzpicture}
  \end{figure}

\end{frame}


\section{SDR Devices}

\begin{frame}

  \frametitle{SDR Devices}

  \begin{itemize}
  \item Ettus Research USRP B200-mini
  \item LimeSDR Mini
  \item Airspy R2
  \item Analog Devices PlutoSDR
  \item RTL-SDR v3
  \end{itemize}

  \begin{figure}[H]
    \centering
    \includegraphics[height=0.5\textheight]{images/sdr-devices.jpg}
    \caption*{\scriptsize{SDR Devices attached to testbed}}
  \end{figure}

\end{frame}


\section{Programmable RF Switch}

\begin{frame}

  \frametitle{Programmable RF Switch}

  \begin{figure}[H]
    \centering
    \includegraphics[height=0.72\textheight]{images/programmable-rf-switch.jpg}
    \caption*{\scriptsize{SDR Testbed Programmable RF Switch \\ \emph{Mini-Circuits RC-2SP4T-A18}}}
  \end{figure}

\end{frame}


\section{Programmable Attenuator}

\begin{frame}

  \frametitle{Programmable Attenuator}

  \begin{figure}[H]
    \centering
    \includegraphics[height=0.72\textheight]{images/programmable-attenuator.jpg}
    \caption*{\scriptsize{SDR Testbed Programmable Attenuator \\ \emph{Mini-Circuits RCDAT-6000--90}}}
  \end{figure}

\end{frame}


\section{Connections Diagram}

\begin{frame}

  \frametitle{Connections Diagram}

  \begin{figure}[H]
    \centering
    \begin{circuitikz}[american resistors,auto,scale=0.6,transform shape]
      \tikzset{
        rectroundnode/.style={rectangle,rounded corners,draw=black,very thick,inner sep=0.5em,minimum height=2em,text width=1cm,text centered}
      }
      \draw (0,0) node[spdt](sw1a){} (sw1a.north west) node{Out 1\textsubscript{A}};
      \draw (2,0) node[spdt,yscale=-1](sw2a){} (sw2a.south west) node{Out 2\textsubscript{A}};
      \draw (4,0) node[spdt,yscale=-1](sw3a){} (sw3a.south west) node{Out 3\textsubscript{A}};
      \draw (6,0) node[spdt,yscale=-1](sw4a){} (sw4a.south west) node{Out 4\textsubscript{A}};
      \draw (sw1a.out 2) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw2a.out 1) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw3a.out 1) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw4a.out 1) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw1a.out 1) |- ++(0,1) to [short,-*] ++(2,0) to [short,-*] ++(2,0) -- ++(2,0) node[circ](ina){} -| (sw4a.out 2);
      \draw (sw2a.out 2) -- ++(0,1);
      \draw (sw3a.out 2) -- ++(0,1);

      \node[rectroundnode,below=1.5cm of sw1a.in](sdr1){\small SDR \#1};
      \node[rectroundnode,below=1.5cm of sw2a.in](sdr2){\small SDR \#2};
      \node[rectroundnode,below=1.5cm of sw3a.in](sdr3){\small SDR \#3};
      \node[rectroundnode,below=1.5cm of sw4a.in](sdr4){\small SDR \#4};
      \draw (sdr1.north) to [short] (sw1a.in);
      \draw (sdr2.north) to [short] (sw2a.in);
      \draw (sdr3.north) to [short] (sw3a.in);
      \draw (sdr4.north) to [short] (sw4a.in);

      \draw (0,-6) node[spdt](sw1b){} (sw1b.north west) node{Out 1\textsubscript{B}};
      \draw (2,-6) node[spdt,yscale=-1](sw2b){} (sw2b.south west) node{Out 2\textsubscript{B}};
      \draw (4,-6) node[spdt,yscale=-1](sw3b){} (sw3b.south west) node{Out 3\textsubscript{B}};
      \draw (6,-6) node[spdt,yscale=-1](sw4b){} (sw4b.south west) node{Out 4\textsubscript{B}};
      \draw (sw1b.out 2) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw2b.out 1) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw3b.out 1) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw4b.out 1) to [/tikz/circuitikz/bipoles/length=20pt,resistor,l_=50<\ohm>] ++(0,-1) node[/tikz/circuitikz/bipoles/length=20pt,ground]{};
      \draw (sw1b.out 1) |- ++(0,1) to [short,-*] ++(2,0) to [short,-*] ++(2,0) -- ++(2,0) node[circ](inb){} -| (sw4b.out 2);
      \draw (sw2b.out 2) -- ++(0,1);
      \draw (sw3b.out 2) -- ++(0,1);

      \draw (ina) to [short,l^=In\textsubscript{A}] ++(1,0) to [resistor,o-,l=fixed att.] ++(0,-3) to [variable resistor,-o,l=prog. att.] ++(0,-3) to [short,-o,l^=In\textsubscript{B}] (inb);

      \node[rectroundnode,below=1.5cm of sw1b.in](sdr5){\small SDR \#5};
      \node[rectroundnode,below=1.5cm of sw2b.in](sdr6){\small SDR \#6};
      \node[rectroundnode,below=1.5cm of sw3b.in](sdr7){\small SDR \#7};
      \node[rectroundnode,below=1.5cm of sw4b.in](sdr8){\small SDR \#8};
      \draw (sdr5.north) to [short] (sw1b.in);
      \draw (sdr6.north) to [short] (sw2b.in);
      \draw (sdr7.north) to [short] (sw3b.in);
      \draw (sdr8.north) to [short] (sw4b.in);
    \end{circuitikz}
    \medskip
    \caption*{\scriptsize{Programmable RF switch connection diagram}}
  \end{figure}

\end{frame}


\section{Software Technologies and Components}

\begin{frame}

  \frametitle{Software Technologies and Components}

  \begin{minipage}{0.4\textwidth}
    \begin{itemize}
    \item CentOS Linux OS
    \item Ansible
    \item GitLab CI
    \item Docker
    \item GNU Radio
    \item Robot Framework
    \end{itemize}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \includegraphics[width=0.3\textwidth]{images/centos-logo.png}
    \includegraphics[width=0.3\textwidth]{images/ansible-logo.png}
    \includegraphics[width=0.3\textwidth]{images/gitlab-logo.png}
    \includegraphics[width=0.35\textwidth]{images/docker-logo.png}
    \includegraphics[width=0.23\textwidth]{images/gnuradio-logo.png}
    \includegraphics[width=0.3\textwidth]{images/robot-framework-logo.png}
  \end{minipage}

\end{frame}


\section{Software Installation}

\begin{frame}[fragile]

  \frametitle{Software Installation}

  \begin{minipage}{0.65\textwidth}
    \begin{itemize}
    \item CentOS Minimal x86--64
    \item Ansible roles
      \begin{itemize}
      \item Docker
      \item GitLab CI Runner
      \end{itemize}
    \item Configuration variables
    \end{itemize}
    \medskip
    \begin{lstlisting}[basicstyle=\ttfamily\scriptsize]
---
gitlab_runner:
  runners:
    - executor: 'docker'
      name: 'sdr-testbed'
      url: 'https://gitlab.com/'
      registration_token: '[redacted]'
      docker_image: 'alpine'
      tag_list: 'sdr-testbed'
    \end{lstlisting}
  \end{minipage}
  \begin{minipage}{0.3\textwidth}
    \begin{figure}[H]
      \centering
      \includegraphics[width=1\textwidth]{images/sdr-testbed-runner.png}
      \caption*{\scriptsize{SDR Testbed GitLab CI runner}}
    \end{figure}
  \end{minipage}

\end{frame}


\section{Software Block Diagram}

\begin{frame}

  \frametitle{Software Block Diagram}

  \begin{figure}[H]
    \centering
    \begin{tikzpicture}[node distance=1cm,auto,scale=0.8,transform shape]
      \tikzset{
        rectroundnode/.style={rectangle,rounded corners,draw=black,very thick,inner sep=0.8em,minimum height=3em,text width=2.5cm,text centered},
        rectfit/.style={rounded corners,draw=black,very thick,inner sep=0.8em,minimum height=3em,text width=2.5cm,text centered},
        arrowlabel/.style={text width=7em},
        arrow/.style={<->,>=latex',shorten >=1pt,thick}
      }
      \node[rectroundnode,minimum width=10em](userinterface){GitLab CI};
      \node[rectroundnode,right=3.5em of userinterface,minimum width=10em](jobserver){GitLab CI Runner};
      \node[rectroundnode,below=of jobserver,minimum width=7em](automation){Robot Framework};
      \node[rectroundnode,right=of automation,minimum width=7em](testbedconfig){SDR Testbed configuration library};
      \node[rectroundnode,below=of automation,minimum width=7em](sdrplatform){GNU Radio};
      \node[rectroundnode,minimum width=10em,minimum height=8em,below=of userinterface](storage){GitLab CI job artifacts storage};
      \node[rectfit,fit=(automation)(testbedconfig)(sdrplatform),dashed,label={below:Docker container}](container){};

      \draw[arrow](userinterface.east) -- node[arrowlabel,above,text centered]{\small job \\ \& data} (jobserver.west);
      \draw[arrow](jobserver.south) -- node[arrowlabel,right]{\small scenario \& data} (automation.north);
      \draw[arrow,->](automation.east) -- node[arrowlabel,above,text centered]{\small config} (testbedconfig.west);
      \draw[arrow](automation.south) -- node[arrowlabel,right]{\small control \\ \& data} (sdrplatform.north);
      \draw[arrow,->](userinterface.south) -- node[arrowlabel,right]{\small experiment results} (storage.north);
    \end{tikzpicture}
  \end{figure}

\end{frame}


\section{Challenges}

\begin{frame}

  \frametitle{Challenges}

  \begin{itemize}
  \item USB device access
    \begin{itemize}
    \item Access to USB host devices within the container
    \item SDR USB devices stalling
    \end{itemize}
  \item Scalibility
    \begin{itemize}
    \item Scale-up --- Multiple GitLab CI runners on same host
    \item Scale-out --- Multiple hosts with single GitLab CI runners
    \end{itemize}
  \item Configurability
    \begin{itemize}
    \item SDR RX --- TX combinations --- Daisy-chaining RF switches
    \end{itemize}
  \item Performance
    \begin{itemize}
    \item Network upload bottleneck
    \end{itemize}
  \end{itemize}

\end{frame}


\section{Applications and Future Work}

\begin{frame}

  \frametitle{Applications and Future Work}

  \begin{itemize}
  \item Testing of GNU Radio flowgraphs (e.g.\ SatNOGS Flowgraphs)
  \item E2E testing of end-user applications (e.g.\ SatNOGS Client)
  \item Re-purpose for HIL testing (e.g.\ SatNOGS COMMS)
  \end{itemize}

\end{frame}


\section{Links / References}

\begin{frame}

  \frametitle{Links / References}

  \begin{itemize}
  \item \href{https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed}{SDR Testbed repository}
  \item \href{https://docs.gitlab.com/ee/ci/}{GitLab CI documentation}
  \item \href{https://gitlab.com/librespacefoundation/docker-gnuradio}{SDR Testbed GNU Radio Docker image}
  \item \href{https://gitlab.com/librespacefoundation/ops/lsf-ansible}{Libre Space Foundation Ansible Playbook}
  \item \href{https://gitlab.com/librespacefoundation/ops/sdr-testbed-runners}{SDR Testbed Runners repository}
  \item \href{https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed}{\LaTeX\ source of this presentation}
  \end{itemize}

\end{frame}


\section{Thank you}

\begin{frame}

  \frametitle{Thank you}

  Questions?

\end{frame}

\end{document}
