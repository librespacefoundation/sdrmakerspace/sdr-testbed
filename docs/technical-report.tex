%% SDR Testbed - Technical Report
%% Copyright (C) 2020, 2021 Libre Space Foundation
%%
%% This work is licensed under a
%% Creative Commons Attribution-ShareAlike 4.0 International License.
%%
%% You should have received a copy of the license along with this
%% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

\documentclass[english,titlepage,a4paper]{report}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage{bookmark}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage[siunitx]{circuitikz}
\usetikzlibrary{arrows,positioning,fit}
\usepackage[toc,acronyms,section]{glossaries}
\usepackage{textcomp}
\usepackage{titlesec}
\usepackage{caption}

\input{include/version}
\input{include/requirements}

%% Glossary terms and acronyms
\newglossarystyle{definitions}{
  \setglossarystyle{long}
  \renewenvironment{theglossary}{
    \begin{longtable}{
        p{3cm}
        p{\glsdescwidth}
      }
    }{
    \end{longtable}
  }
}
\newglossary*{definitions}{Definitions}

\newglossarystyle{references}{
  \setglossarystyle{long}
  \renewenvironment{theglossary}{
    \begin{longtable}{
        p{3cm}
        p{\glsdescwidth}
      }
    }{
    \end{longtable}
  }
}
\newglossary*{references}{References}

\makeglossaries{}

\input{glossaries/acronyms}
\input{glossaries/terms}

\title{
  Software Defined Radio Testbed \\
  \large Technical Report
}
\author{Vasilis Tsiligiannis}
\date{\today\\\version{}}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}
\renewcommand*{\arraystretch}{2}
\maketitle
\tableofcontents


\chapter{Introduction}

The Software Defined Radio Testbed is a tool for the research and development of \acrshort{SDR}-based satellite communication systems.
The testbed can be used for early experimentation and testing of new algorithms and configurations.
The objective of this project was to physically implement such a testbed and make it available to Software Defined Radio developers and researchers.
The testbed has been developed based on COTS hardware and, free and open source software.
Being itself free and open source, it is freely and publicly available as a service to the open source \acrshort{SDR} community.


{\let\clearpage\relax \chapter{Implementation}}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{images/sdr-testbed.jpg}
  \caption*{SDR Testbed RF Components}\label{fig:sdr-testbed}
\end{figure}

The development of an \gls{SDR Testbed} system spanned from software development and hardware assembly to integration of various \acrshort{SDR} toolchains and components.
The \gls{SDR Testbed} was assembled at Libre Space Foundation laboratory in Athens, Greece.
A powerful COTS \gls{x86-64} computer was assembled based on an AMD Ryzen 7 2700X Eight-Core Processor and used as a host platform for the testbed. % chktex 8
The platform resources were carefully selected so that they could accommodate the interfaces and system requirements of the \acrshort{SDR} devices defined during testbed requirements specification.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{images/sdr-devices.jpg}
  \caption*{SDR Devices attached to testbed}\label{fig:sdr-devices}
\end{figure}

The \acrshort{SDR} devices currently attached and controlled by the host computer are:
\begin{itemize}
\item Ettus Research USRP B200-mini
\item \gls{LimeSDR Mini}
\item Airspy R2
\item Analog Devices \gls{PlutoSDR}
\item \gls{RTL-SDR v3}
\end{itemize}

The \gls{SDR Testbed} hardware can be re-configured upon request to support the full list of \acrshort{SDR} devices defined in the requirements specification.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.72\textwidth]{images/programmable-rf-switch.jpg}
  \caption*{SDR Testbed Programmable RF Switch}\label{fig:programmable-rf-switch}
\end{figure}

The \gls{RC-2SP4T-A18} switch from Mini-Circuits physically connects the selected pair of \acrshort{SDR} devices.
The switch is controlled via \gls{Ethernet} using the \gls{SCPI} protocol.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.72\textwidth]{images/programmable-attenuator.jpg}
  \caption*{SDR Testbed Programmable Attenuator}\label{fig:programmable-attenuator}
\end{figure}

The programmable \gls{RCDAT-6000-90} as well as the fixed \gls{BW-S20W2+} attenuators from Mini-Circuits are connected in series and attenuate \acrshort{TX} signals to safe \acrshort{RX} input levels. % chktex 8
The programmable attenuator is also controlled via \gls{Ethernet} using the \gls{SCPI} protocol.
The level of attenuation can be controlled during an \gls{experiment} for cases in which losses need to be emulated.

\newpage
The host platform operating system is \gls{CentOS} \gls{Linux} 7 and runs \gls{Docker} engine and a \gls{GitLab} \acrshort{CI} runner.
The runner is registered to \gls{GitLab}~\cite{sdr-testbed-runners} and pulls queued \glspl{job} of \glspl{experiment}.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{images/sdr-testbed-runner.png}
  \caption*{SDR Testbed GitLab CI runner}\label{fig:sdr-testbed-runner}
\end{figure}

All software running on the host platform is provisioned with \gls{Ansible}~\cite{lsf-ansible} to ensure reproducibility.
A \gls{Docker} image~\cite{docker-gnuradio} has been developed as the software platform for which the \glspl{experiment} run.
The image includes drivers for the supported \gls{SDR} devices as well as the \gls{GNU Radio} platform, accompanied modules and \gls{Robot Framework}.


\chapter{Requirements Compliance}

\autoref{tab:req-comp} shows compliance of implementation with requirements defined during specification.

{\footnotesize
  \begin{longtable}{rcp{0.7\linewidth}}
    \caption{Requirements Compliance}\label{tab:req-comp} \\
    \toprule
    \textbf{REQ}      & \textbf{Compliance} & \textbf{Comment}                                                          \\ \toprule
    \endfirsthead{}
    %
    \endhead{}
    %
    \requrl{24}{0001} & Full                &                                                                           \\ \midrule
    \requrl{25}{0002} & Full                &                                                                           \\ \midrule
    \requrl{26}{0003} & Full                &                                                                           \\ \midrule
    \requrl{27}{0004} & Full                &                                                                           \\ \midrule
    \requrl{28}{0005} & Full                &                                                                           \\ \midrule
    \requrl{29}{0006} & Full                &                                                                           \\ \midrule
    \requrl{30}{0007} & Full                &                                                                           \\ \midrule
    \requrl{31}{0008} & Full                &                                                                           \\ \midrule
    \requrl{32}{0009} & Full                &                                                                           \\ \midrule
    \requrl{33}{0010} & Full                &                                                                           \\ \midrule
    \requrl{34}{0011} & Full                &                                                                           \\ \midrule
    \requrl{35}{0012} & Full                &                                                                           \\ \midrule
    \requrl{36}{0013} & Full                &                                                                           \\ \midrule
    \requrl{37}{0014} & Full                &                                                                           \\ \midrule
    \requrl{38}{0015} & Partial             & Can be compiled during \gls{job} execution but not Debian packaged        \\ \midrule
    \requrl{39}{0016} & Full                &                                                                           \\ \midrule
    \requrl{40}{0017} & Partial             & \gls{SCPI} utilities available but not wrapped around convenient keywords \\ \midrule
    \requrl{41}{0018} & None                & Requirement dropped as superfluous for this application                   \\ \midrule
    \requrl{42}{0019} & Full                &                                                                           \\ \midrule
    \requrl{43}{0020} & Full                &                                                                           \\ \midrule
    \requrl{44}{0021} & Full                &                                                                           \\ \midrule
    \requrl{45}{0022} & Partial             & \gls{HDF5} library available but not wrapped around convenient keyword    \\ \midrule
    \requrl{46}{0023} & Full                &                                                                           \\ \midrule
    \requrl{47}{0024} & Full                &                                                                           \\ \midrule
    \requrl{48}{0025} & Full                &                                                                           \\ \midrule
    \requrl{49}{0101} & Full                &                                                                           \\ \midrule
    \requrl{50}{0102} & Full                &                                                                           \\ \midrule
    \requrl{51}{0103} & Full                &                                                                           \\ \midrule
    \requrl{52}{0201} & Full                &                                                                           \\ \bottomrule
  \end{longtable}
}



\chapter{Conclusion}

The \gls{SDR Testbed} is currently operational 24/7 at Libre Space Foundation laboratory.
The testbed is available to anyone (upon request) through \gls{GitLab} \acrshort{CI} and can become an invaluable tool to any researcher or \acrshort{SDR} developer.
It is planned to be used in various Libre Space Foundation projects like SatNOGS, SatNOGS COMMS and SIDLOC, effectively acting as a \acrshort{HIL} testing platform and reducing frequency of software errors which are costly to fix after a release.


{\let\clearpage\relax \chapter{Future Work}}

The \gls{SDR Testbed} can be further developed and improved, both in term of software, hardware and infrastructure.
In terms of infrastructure, it proved that network bandwidth plays a key role to \gls{job} throughput, especially for \glspl{experiment} which produce a large amount of data (e.g. \gls{I/Q data}).
Upgrading the internet connectivity upload bandwidth of the laboratory should be considered in the future.

Further enhancing \gls{Robot Framework} libraries and implementing more wrappers around control libraries and utilities should also be considered as future work in order to make the testbed even more user friendly.

Finally, well-organized user and developer documentation needs to be written based on the specification and low-level documentation which is already available.


\chapter*{Appendix I:\@ Glossary and Acronyms}
\addcontentsline{toc}{chapter}{Appendix I:\@ Glossary and Acronyms}

\printglossary[type=definitions]
\printglossary[type=\acronymtype]
\printglossary[type=references]
\printglossary{}

\bibliographystyle{unsrt}
\bibliography{references}

\end{document}
