# Software Defined Radio Testbed #

Documentation, software and hardware for building an SDR Testbed.

## Documentation ##

- Requirements Specification
  - [Unstable](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/master/raw/docs/build/specification.pdf?job=docs_devel)
  - Stable
    - [1.1.0](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/1.1.0/raw/docs/build/specification.pdf?job=docs_release)
    - [1.0.2](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/1.0.2/raw/doc/build/specification.pdf?job=build)
    - [1.0.1](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/1.0.1/raw/doc/build/specification.pdf?job=build)
    - [1.0.0](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/1.0.0/raw/doc/build/specification.pdf?job=build)
- Technical Report
  - [Unstable](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/master/raw/docs/build/technical-report.pdf?job=docs_devel)
  - Stable
    - [1.1.0](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/1.1.0/raw/docs/build/technical-report.pdf?job=docs_release)
- Presentation
  - [Unstable](https://gitlab.com/librespacefoundation/sdrmakerspace/sdr-testbed/-/jobs/artifacts/master/raw/docs/build/presentation.pdf?job=docs_devel)
